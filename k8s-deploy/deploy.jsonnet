local is_production = std.extVar('IS_PRODUCTION') == 'true';
local namespace = std.extVar('KUBE_NAMESPACE');
local base_domain = std.extVar('BASE_DOMAIN');

[
  {
    kind: 'Secret',
    apiVersion: 'v1',
    metadata: {
      name: 'pull-credentials',
    },
    data: {
      '.dockerconfigjson': std.base64(std.manifestJson({
        auths: {
          [std.extVar('CI_REGISTRY')]: {
            auth: std.base64(std.extVar('CI_DEPLOY_USER') + ':' + std.extVar('CI_DEPLOY_PASSWORD'))
          },
        },
      })),
    },
    type: 'kubernetes.io/dockerconfigjson',
  },

] + (if is_production then [
  {
    apiVersion: 'v1',
    kind: 'PersistentVolumeClaim',
    metadata: {
      name: 'launchserver-files-volume',
    },
    spec: {
      storageClassName: 'local-path',
      accessModes: [
        'ReadWriteOnce'
      ],
      resources: {
        requests: {
          storage: '30Gi'
        },
      },
    },
  },

  {
    apiVersion: 'v1',
    kind: 'PersistentVolumeClaim',
    metadata: {
      name: 'launchserver-clients-volume',
    },
    spec: {
      storageClassName: 'local-path',
      accessModes: [
        'ReadWriteOnce'
      ],
      resources: {
        requests: {
          storage: '100Mi'
        },
      },
    },
  },
] else [
  {
    apiVersion: 'v1',
    kind: 'PersistentVolumeClaim',
    metadata: {
      name: 'launchserver-files-volume',
    },
    spec: {
      accessModes: [
        'ReadWriteOnce'
      ],
      resources: {
        requests: {
          storage: '1Gi'
        },
      },
    },
  },

  {
    apiVersion: 'v1',
    kind: 'PersistentVolumeClaim',
    metadata: {
      name: 'launchserver-clients-volume',
    },
    spec: {
      accessModes: [
        'ReadWriteOnce'
      ],
      resources: {
        requests: {
          storage: '100Mi'
        },
      },
    },
  },
]) + [
  {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: 'launchserver',
      labels: {
        app: 'launchserver'
      },
      annotations: {
        'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
        'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
      }
    },
    spec: {
      replicas: 1,
      selector: {
        matchLabels: {
          app: 'launchserver',
        },
      },
      template: {
        metadata: {
          labels: {
            app: 'launchserver',
          },
          annotations: {
            'app.gitlab.com/app': std.extVar('CI_PROJECT_PATH_SLUG'),
            'app.gitlab.com/env': std.extVar('CI_ENVIRONMENT_SLUG'),
          }
        },
        spec: {
          containers: [
            {
              image: '%s:%s' % [std.extVar('CI_REGISTRY_IMAGE'), std.extVar('CI_COMMIT_SHA')],
              name: 'launchserver',
              env: [
                {
                  name: 'MINIO_ENDPOINT',
                  value: 'minio.minio'
                },
                {
                  name: 'MINIO_PORT',
                  value: '9000'
                },
                {
                  name: 'MINIO_USE_SSL',
                  value: '0'
                },
                {
                  name: 'MINIO_ACCESS_KEY',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'minio-creds-secret',
                      key: 'accesskey'
                    },
                  },
                },
                {
                  name: 'MINIO_SECRET_KEY',
                  valueFrom: {
                    secretKeyRef: {
                      name: 'minio-creds-secret',
                      key: 'secretkey'
                    },
                  },
                },
                {
                  name: 'MINIO_PUBLIC_URL',
                  value: 'https://storage.c7x.dev'
                },
                {
                  name: 'DATABASE_HOST',
                  value: 'postgres.common-services'
                },
                {
                  name: 'REDIS_URL',
                  value: 'redis://redis.common-services:6379/'
                },
                {
                  name: 'NODE_TLS_REJECT_UNAUTHORIZED',
                  value: '0'
                }
              ],
              volumeMounts: [
                {
                  name: 'launchserver-clients-volume',
                  mountPath: '/app/clients',
                  readOnly: true
                },
                {
                  name: 'launchserver-files-volume',
                  mountPath: '/app/files',
                  readOnly: true
                },
              ],
              ports: [
                {
                  containerPort: 8080,
                  protocol: 'TCP',
                },
              ],
            },
          ],
          volumes: [
            {
              name: 'launchserver-clients-volume',
              persistentVolumeClaim: {
                claimName: 'launchserver-clients-volume',
              },
            },
            {
              name: 'launchserver-files-volume',
              persistentVolumeClaim: {
                claimName: 'launchserver-files-volume',
              },
            },
          ],
          imagePullSecrets: [
            {
              name: 'pull-credentials'
            },
          ],
        },
      },
    },
  },

  {
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: 'launchserver',
    },
    spec: {
      selector: {
        app: 'launchserver',
      },
      ports: [
        {
          name: 'http',
          protocol: 'TCP',
          port: 80,
          targetPort: 8080,
        }
      ],
    },
  },
] + (if is_production then [
  {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: 'backend',
    },
    spec: {
      rules: [
        {
          host: base_domain,
          http: {
            paths: [
              {
                path: '/',
                pathType: 'Prefix',
                backend: {
                  service: {
                    name: 'launchserver',
                    port: {
                      number: 80
                    },
                  },
                },
              },
            ],
          },
        },
      ],
    },
  },
] else [
  {
    apiVersion: 'networking.k8s.io/v1',
    kind: 'Ingress',
    metadata: {
      name: 'launchserver',
      annotations: {
        'cert-manager.io/cluster-issuer': 'letsencrypt',
        'nginx.ingress.kubernetes.io/rewrite-target': '/$1'
      },
    },
    spec: {
      tls: [
        {
          hosts: [
            base_domain
          ],
          secretName: 'ssl-secret',
        },
      ],
      rules: [
        {
          host: base_domain,
          http: {
            paths: [
              {
                path: '/launcher-dev/(.*)',
                pathType: 'ImplementationSpecific',
                backend: {
                  service: {
                    name: 'launchserver',
                    port: {
                      number: 80
                    },
                  },
                },
              },
            ],
          },
        },
      ],
    },
  },
]) + [

]