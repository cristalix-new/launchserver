import {RequestHandlingError} from "./RequestHandlingError";

export interface LauncherRequest<T> {
    params: Record<string, string>;
    data: T;
}

import {Runtype, ValidationError} from "runtypes";

export abstract class Handler<TRequest, TResponse> {

    protected constructor(private readonly requestTypeChecker?: Runtype<TRequest>) {
    }

    async handleRaw(params: Record<string, string>, data: unknown): Promise<TResponse> {
        let resultData = <TRequest>data;
        if (this.requestTypeChecker !== undefined) {
            try {
                resultData = this.requestTypeChecker.check(data);
            } catch (e) {
                if (e instanceof ValidationError) {
                    throw new RequestHandlingError(`request-data-error: ${e.message}`);
                }
                throw e;
            }
        }
        return await this.handle({
            params,
            data: resultData
        });
    }

    protected abstract handle(request: LauncherRequest<TRequest>): Promise<TResponse>;
}