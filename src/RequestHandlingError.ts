export class RequestHandlingError extends Error {

    constructor(message: string, public readonly errorCode = 400) {
        super(message);
    }
}