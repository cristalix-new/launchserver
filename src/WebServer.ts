import express from "express";

import {Express} from "express";
import {Handler} from "./Handler";
import {RequestHandlingError} from "./RequestHandlingError";
import bodyParser from "body-parser";

export class WebServer {

    private readonly webServer: Express;

    constructor() {
        this.webServer = express();
        this.webServer.set("x-powered-by", false);
        this.webServer.set("etag", false);
        this.webServer.use(bodyParser.json());
    }

    addHandler(url: string, method: string, handler: Handler<unknown, unknown>): void {
        this.webServer.use(url, async (req, res, next) => {
            if (req.method !== method) {
                next();
                return;
            }
            try {
                res.json({
                    success: true,
                    data: await handler.handleRaw(req.params, req.body)
                });
            } catch (error) {
                if (error instanceof RequestHandlingError) {
                    res.status(error.errorCode).json({
                        success: false,
                        error: error.message
                    });
                    return;
                }

                console.error(`An error was thrown on ${req.method} request to ${req.path}: ${error}`);

                res.status(500).json({
                    success: false,
                    error: "internal-server-error",
                    whoFuckedUp: "radioegor146"
                });
            }
        });
    }

    start(port: number): void {
        this.webServer.use((req, res) => {
            res.status(400).json({
                success: false,
                error: `${req.method} handler for ${req.url} does not exist`
            });
        });
        this.webServer.listen(port);
    }
}