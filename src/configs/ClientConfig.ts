import {Number, Record, String, Array, Static, Boolean} from "runtypes";

const ConfigType = Record({
    title: String,
    id: String,
    serverOnlineId: String,
    imageId: String,
    directory: String,
    version: String,
    assetsAdditionsDirectory: String,
    sortIndex: Number,
    information: String,
    supportedSignals: Array(String),
    files: Record({
        verify: Array(String)
    }),
    access: Record({
        technical: Boolean,
        allowedUuids: Array(String)
    }),
    runConfig: Record({
        mainClass: String,
        classPath: Array(String),
        jvmArgs: Array(String),
        clientArgs: Array(String)
    }),
    lastNews: Record({
        imageUrl: String,
        text: String,
        title: String
    })
});

export type ClientConfig = Static<typeof ConfigType>

export function decodeConfig(config: unknown): ClientConfig {
    return ConfigType.check(config);
}