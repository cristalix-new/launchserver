import {Handler, LauncherRequest} from "../Handler";
import {RequestHandlingError} from "../RequestHandlingError";
import {AuthError, AuthService} from "../services/AuthService";
import {TokenService} from "../services/TokenService";
import {Null, Record, Static, String, Undefined, Union} from "runtypes";
import {Uuid} from "../helpers/utils";

const AuthRequestType = Record({
    username: String,
    password: String,
    otpCode: Union(String, Undefined, Null)
});

type AuthRequest = Static<typeof AuthRequestType>;

interface AuthResponse {
    readonly refreshToken: string;
}

export class AuthHandler extends Handler<AuthRequest, AuthResponse> {

    constructor(private readonly tokenService: TokenService, private readonly authService: AuthService) {
        super(AuthRequestType);
    }

    async handle(request: LauncherRequest<AuthRequest>): Promise<AuthResponse> {
        let uuid: Uuid | null = null;
        try {
            uuid = await this.authService.auth(request.data.username, request.data.password,
                request.data.otpCode || undefined);
        } catch (e) {
            if (e instanceof AuthError) {
                throw new RequestHandlingError(e.message, 401);
            }

            throw e;
        }
        if (!uuid) {
            throw new RequestHandlingError("wrong-username-or-password", 400);
        }
        return {
            refreshToken: this.tokenService.createToken({
                uuid: uuid,
                purpose: "refresh"
            })
        };
    }
}