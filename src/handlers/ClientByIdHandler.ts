import {Handler, LauncherRequest} from "../Handler";
import {LaunchServerService} from "../services/LaunchServerService";
import {RequestHandlingError} from "../RequestHandlingError";
import {Record, Static, String} from "runtypes";
import {TokenService} from "../services/TokenService";

const ClientByIdRequestType = Record({
    accessToken: String,
    clientId: String
});

type ClientByIdRequest = Static<typeof ClientByIdRequestType>;

interface AssetFile {
    readonly downloadUrl: string;
    readonly path: string;
    readonly sha256: string;
}

interface ClientFile {
    readonly downloadUrl: string;
    readonly path: string;
    readonly xxHash: string;
}

interface ClientByNameResponse {
    readonly clientFiles: ClientFile[];
    readonly assetFiles: AssetFile[];
    readonly version: string;
    readonly verifiedFiles: string[];
    readonly classPathFiles: string[];
    readonly mainClass: string;
    readonly jvmArgs: string[];
    readonly supportedSignals: string[];
}

export class ClientByIdHandler extends Handler<ClientByIdRequest, ClientByNameResponse> {

    constructor(private readonly launchServerService: LaunchServerService, private readonly tokenService: TokenService) {
        super(ClientByIdRequestType);
    }

    async handle(request: LauncherRequest<ClientByIdRequest>): Promise<ClientByNameResponse> {
        const token = this.tokenService.getTokenData(request.data.accessToken);
        if (!token || token.purpose !== "access") {
            throw new RequestHandlingError("wrong-token", 401);
        }

        const clientId = request.data.clientId;

        const client = this.launchServerService.getClientById(clientId);

        if (!client) {
            throw new RequestHandlingError("client-not-found", 404);
        }

        if (client.config.access.technical && !client.config.access.allowedUuids.find(uuid => token.uuid === uuid)) {
            throw new RequestHandlingError("client-not-found", 404);
        }

        return {
            clientFiles: client.clientFiles,
            assetFiles: client.assetFiles,
            verifiedFiles: client.config.files.verify,
            classPathFiles: client.config.runConfig.classPath,
            version: client.config.version,
            mainClass: client.config.runConfig.mainClass,
            jvmArgs: client.config.runConfig.jvmArgs,
            supportedSignals: client.config.supportedSignals
        };
    }
}