import {Handler, LauncherRequest} from "../Handler";
import {LaunchServerService} from "../services/LaunchServerService";
import {NewsService} from "../services/NewsService";
import {ServerOnlineService} from "../services/ServerOnlineService";
import {Record, Static, String} from "runtypes";
import {RequestHandlingError} from "../RequestHandlingError";
import {TokenService} from "../services/TokenService";

const ClientsRequestType = Record({
    accessToken: String
});

type ClientsRequest = Static<typeof ClientsRequestType>;

interface ClientNewsData {
    readonly imageUrl: string;
    readonly text: string;
    readonly title: string;
}

interface ClientInfoData {
    readonly id: string;
    readonly imageId: string;
    readonly title: string;
    readonly online: boolean;
    readonly currentPlayers: number;
    readonly maxPlayers: number;
    readonly information: string;
    readonly technical: boolean;
    readonly lastNews: ClientNewsData;
}

interface ClientsResponse {
    readonly clients: ClientInfoData[];
}

export class ClientsHandler extends Handler<ClientsRequest, ClientsResponse> {

    constructor(private readonly launchServerService: LaunchServerService, private readonly newsService: NewsService,
                private readonly serverOnlineService: ServerOnlineService, private readonly tokenService: TokenService) {
        super(ClientsRequestType);
    }

    async handle(request: LauncherRequest<ClientsRequest>): Promise<ClientsResponse> {
        const token = this.tokenService.getTokenData(request.data.accessToken);
        if (!token || token.purpose !== "access") {
            throw new RequestHandlingError("wrong-token", 401);
        }
        return {
            clients: await Promise.all(this.launchServerService.getClients().filter(client => !client.config.access.technical ||
                client.config.access.allowedUuids.find(uuid => token.uuid == uuid)).sort((clientA, clientB) =>
                clientA.config.sortIndex < clientB.config.sortIndex ? -1 :
                    (clientA.config.sortIndex === clientB.config.sortIndex ? 0 : 1))
                .map(async client => {
                    const online = await this.serverOnlineService.getServerOnline(client.config.serverOnlineId);

                    return {
                        id: client.config.id,
                        imageId: client.config.imageId,
                        title: client.config.title,
                        online: online.online,
                        currentPlayers: online.currentPlayers,
                        maxPlayers: online.maxPlayers,
                        information: client.config.information,
                        lastNews: client.config.lastNews,
                        technical: client.config.access.technical
                    };
                }))
        };
    }
}
