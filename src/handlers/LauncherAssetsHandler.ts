import {Handler} from "../Handler";
import {StorageService} from "../services/StorageService";
import {LaunchServerService} from "../services/LaunchServerService";

interface LauncherAssetsResponse {
    readonly url: string;
    readonly hash: string;
}

export class LauncherAssetsHandler extends Handler<void, LauncherAssetsResponse> {

    constructor(private readonly storageService: StorageService, private readonly launchServerService: LaunchServerService) {
        super();
    }

    protected handle(): Promise<LauncherAssetsResponse> {
        const hash = this.launchServerService.getAssetsHash();
        if (hash === null) {
            throw new Error("no-launcher-found");
        }
        return Promise.resolve({
            url: this.storageService.getDownloadUrl(hash),
            hash
        });
    }
}