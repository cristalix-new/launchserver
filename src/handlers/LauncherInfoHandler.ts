import {Handler} from "../Handler";
import {LaunchServerService} from "../services/LaunchServerService";

interface LauncherInfoResponse {
    readonly downloadUrl: string;
    readonly hash: string;
    readonly size: number;
}

export class LauncherInfoHandler extends Handler<void, LauncherInfoResponse> {

    constructor(private readonly launchServerService: LaunchServerService) {
        super();
    }

    async handle(): Promise<LauncherInfoResponse> {
        return {
            ...this.launchServerService.getLauncherInfo()
        };
    }
}