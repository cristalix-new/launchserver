import {Handler} from "../Handler";
import {NewsService} from "../services/NewsService";

export interface NewsItem {
    readonly timestamp: number;
    readonly imageUrl: string;
    readonly text: string;
    readonly url: string;
}

interface NewsResponse {
    readonly news: NewsItem[];
}

export class NewsHandler extends Handler<void, NewsResponse> {

    constructor(private readonly newsService: NewsService) {
        super();
    }

    async handle(): Promise<NewsResponse> {
        return {
            news: await this.newsService.getLauncherNews()
        };
    }
}