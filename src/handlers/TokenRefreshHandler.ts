import {Handler, LauncherRequest} from "../Handler";
import {RequestHandlingError} from "../RequestHandlingError";
import {TokenService} from "../services/TokenService";
import {Record, Static, String} from "runtypes";
import {ProfileData, ProfileService} from "../services/ProfileService";

const TokenRefreshRequestType = Record({
    refreshToken: String
});

type TokenRefreshRequest = Static<typeof TokenRefreshRequestType>;

type SessionData = {
    readonly accessToken: string;
    readonly profile: ProfileData;
}

interface TokenRefreshResponse {
    readonly session: SessionData;
}

export class TokenRefreshHandler extends Handler<TokenRefreshRequest, TokenRefreshResponse> {

    constructor(private readonly tokenService: TokenService, private readonly profileService: ProfileService) {
        super();
    }

    async handle(request: LauncherRequest<TokenRefreshRequest>): Promise<TokenRefreshResponse> {
        const token = this.tokenService.getTokenData(request.data.refreshToken);
        if (!token || token.purpose !== "refresh") {
            throw new RequestHandlingError("wrong-token", 401);
        }
        return {
            session: {
                accessToken: this.tokenService.createToken({
                    uuid: token.uuid,
                    purpose: "access"
                }),
                profile : (await this.profileService.getProfileByUuid(token.uuid))!,
            }
        };
    }
}