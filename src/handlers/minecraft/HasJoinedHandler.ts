import {Handler, LauncherRequest} from "../../Handler";
import {RequestHandlingError} from "../../RequestHandlingError";
import {MinecraftAuthService} from "../../services/MinecraftAuthService";
import {ProfileData, ProfileService} from "../../services/ProfileService";
import {Record, Static, String} from "runtypes";

const HasJoinedRequestType = Record({
    username: String,
    serverId: String
});

type HasJoinedRequest = Static<typeof HasJoinedRequestType>

export interface HasJoinedResponse {
    readonly profile: ProfileData;
}

export class HasJoinedHandler extends Handler<HasJoinedRequest, HasJoinedResponse> {

    constructor(private readonly minecraftAuthService: MinecraftAuthService,
                private readonly profileService: ProfileService) {
        super(HasJoinedRequestType);
    }

    async handle(request: LauncherRequest<HasJoinedRequest>): Promise<HasJoinedResponse> {
        const uuid = await this.minecraftAuthService.hasJoined(request.data.username, request.data.serverId);
        if (!uuid) {
            throw new RequestHandlingError("failed-to-auth", 401);
        }
        const profile = await this.profileService.getProfileByUuid(uuid);
        if (!profile) {
            throw new RequestHandlingError("failed-to-auth", 401);
        }

        return {
            profile
        };
    }
}