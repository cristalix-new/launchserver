import {Handler, LauncherRequest} from "../../Handler";
import {RequestHandlingError} from "../../RequestHandlingError";
import {TokenService} from "../../services/TokenService";
import {MinecraftAuthService} from "../../services/MinecraftAuthService";
import {Record, Static, String} from "runtypes";

const JoinServerRequestType = Record({
    accessToken: String,
    serverId: String
});

type JoinServerRequest = Static<typeof JoinServerRequestType>;

export class JoinServerHandler extends Handler<JoinServerRequest, void> {

    constructor(private readonly tokenService: TokenService,
                private readonly minecraftAuthService: MinecraftAuthService) {
        super(JoinServerRequestType);
    }

    async handle(request: LauncherRequest<JoinServerRequest>): Promise<void> {
        const token = this.tokenService.getTokenData(request.data.accessToken);
        if (!token || token.purpose !== "access") {
            throw new RequestHandlingError("wrong-token", 401);
        }

        this.minecraftAuthService.joinServer(token.uuid, request.data.serverId);
    }
}