import {Handler, LauncherRequest} from "../../Handler";
import {ProfileData, ProfileService} from "../../services/ProfileService";
import {RequestHandlingError} from "../../RequestHandlingError";
import {UuidService} from "../../services/UuidService";
import {Record, Static, String} from "runtypes";

const ProfileByUsernameRequestType = Record({
    username: String
});

type ProfileByUsernameRequest = Static<typeof ProfileByUsernameRequestType>;

interface ProfileByUsernameResponse {
    readonly profile: ProfileData;
}

export class ProfileByUsernameHandler extends Handler<ProfileByUsernameRequest, ProfileByUsernameResponse> {

    constructor(private readonly uuidService: UuidService, private readonly profileService: ProfileService) {
        super(ProfileByUsernameRequestType);
    }

    async handle(request: LauncherRequest<ProfileByUsernameRequest>): Promise<ProfileByUsernameResponse> {
        const uuid = await this.uuidService.usernameToUuid(request.data.username);

        if (!uuid) {
            throw new RequestHandlingError("player-not-found", 404);
        }

        const profile = await this.profileService.getProfileByUuid(uuid);

        if (!profile) {
            throw new RequestHandlingError("profile-not-found", 404);
        }

        return {
            profile
        };
    }
}