import {Handler, LauncherRequest} from "../../Handler";
import {ProfileData, ProfileService} from "../../services/ProfileService";
import {RequestHandlingError} from "../../RequestHandlingError";
import {Record, Static, String} from "runtypes";

const ProfileByUuidRequestType = Record({
    uuid: String
});

type ProfileByUuidRequest = Static<typeof ProfileByUuidRequestType>;

interface ProfileByUuidResponse {
    readonly profile: ProfileData;
}

export class ProfileByUuidHandler extends Handler<ProfileByUuidRequest, ProfileByUuidResponse> {

    constructor(private readonly profileService: ProfileService) {
        super(ProfileByUuidRequestType);
    }

    async handle(request: LauncherRequest<ProfileByUuidRequest>): Promise<ProfileByUuidResponse> {
        const profile = await this.profileService.getProfileByUuid(request.data.uuid);

        if (!profile) {
            throw new RequestHandlingError("profile-not-found", 404);
        }

        return {
            profile
        };
    }
}