import {Handler, LauncherRequest} from "../../Handler";
import {ProfileData, ProfileService} from "../../services/ProfileService";
import {Record, Array, String, Static} from "runtypes";
import {RequestHandlingError} from "../../RequestHandlingError";

const ProfilesByUsernamesRequestType = Record({
    usernames: Array(String)
});

type ProfilesByUuidsRequest = Static<typeof ProfilesByUsernamesRequestType>;

interface ProfilesByUsernamesResponse {
    readonly profiles: (ProfileData | null)[];
}

export class ProfilesByUsernamesHandler extends Handler<ProfilesByUuidsRequest, ProfilesByUsernamesResponse> {

    constructor(private readonly profileService: ProfileService) {
        super(ProfilesByUsernamesRequestType);
    }

    async handle(request: LauncherRequest<ProfilesByUuidsRequest>): Promise<ProfilesByUsernamesResponse> {
        const usernames = request.data.usernames;
        if (usernames.length > 100) {
            throw new RequestHandlingError("too-many-usernames", 401);
        }
        return {
            profiles: await this.profileService.getProfilesByUsernames(usernames)
        };
    }
}