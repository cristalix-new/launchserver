import {createReadStream} from "fs";
import {createHash} from "crypto";
import {XXHash64} from "xxhash-addon";
import {Stream} from "stream";

export function hashFromFile(filePath: string, algorithm = "sha256"): Promise<string> {
    return hashFromStream(createReadStream(filePath), algorithm);
}

export function hashFromStream(stream: Stream, algorithm = "sha256"): Promise<string> {
    return new Promise((resolve, error) => {
        const hash = createHash(algorithm);
        stream
            .on("data", data => hash.update(data))
            .on("end", () => resolve(hash.digest("hex")))
            .on("error", (e) => error(e));
    });
}

export function xxHashFromFile(filePath: string): Promise<string> {
    return new Promise((resolve, error) => {
        const hash = new XXHash64();
        createReadStream(filePath)
            .on("data", data => hash.update(data))
            .on("end", () => resolve(hash.digest().toString("hex")))
            .on("error", (e) => error(e));
    });
}