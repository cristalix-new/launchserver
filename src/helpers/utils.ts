export type Uuid = string;

export function formatString(string: string, values: Record<string, string>): string {
    for (const key of Object.keys(values)) {
        string = string.replace(`{${key}}`, values[key]);
    }
    return string;
}