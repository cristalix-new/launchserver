import Bluebird from "bluebird";
// eslint-disable-next-line @typescript-eslint/no-explicit-any
global.Promise = <any>Bluebird;

import dotenv from "dotenv";

dotenv.config();

import {WebServer} from "./WebServer";
import {AuthHandler} from "./handlers/AuthHandler";
import {TokenRefreshHandler} from "./handlers/TokenRefreshHandler";
import {NewsHandler} from "./handlers/NewsHandler";
import {ClientsHandler} from "./handlers/ClientsHandler";
import {ClientByIdHandler} from "./handlers/ClientByIdHandler";
import {JoinServerHandler} from "./handlers/minecraft/JoinServerHandler";
import {HasJoinedHandler} from "./handlers/minecraft/HasJoinedHandler";
import {ProfileByUsernameHandler} from "./handlers/minecraft/ProfileByUsernameHandler";
import {ProfileByUuidHandler} from "./handlers/minecraft/ProfileByUuidHandler";
import {ProfilesByUsernamesHandler} from "./handlers/minecraft/ProfilesByUsernamesHandler";
import {LauncherInfoHandler} from "./handlers/LauncherInfoHandler";
import {LaunchServerService} from "./services/LaunchServerService";
import {getRandomJwtSecret, JWTTokenService} from "./services/impl/JWTTokenService";
import {MojangDefaultAssetsService} from "./services/impl/MojangDefaultAssetsService";
import {LocalCacheMinecraftAuthService} from "./services/impl/LocalCacheMinecraftAuthService";
import {MinioStorageService} from "./services/impl/MinioStorageService";
import {VKNewsService} from "./services/impl/VKNewsService";
import {RedisServerOnlineService} from "./services/impl/RedisServerOnlineService";
import {PostgreSQLUuidService} from "./services/impl/PostgreSQLUuidService";
import {PoolingPostgreSQLDatabaseService} from "./services/impl/PoolingPostgreSQLDatabaseService";
import {DefaultProfileService} from "./services/impl/DefaultProfileService";
import {PostgreSQLAuthService} from "./services/impl/PostgreSQLAuthService";
import {LocalTimeoutCacheCachingUuidService} from "./services/impl/LocalTimeoutCacheCachingUuidService";
import {LocalTimeoutCacheCachingProfileImageService} from "./services/impl/LocalTimeoutCacheCachingProfileImageService";
import {HashingStorageProfileImageService} from "./services/impl/HashingStorageProfileImageService";
import {LauncherAssetsHandler} from "./handlers/LauncherAssetsHandler";

const storageService = new MinioStorageService({
    minioClientOptions: {
        endPoint: process.env.MINIO_ENDPOINT || "127.0.0.1",
        port: parseInt(<string>process.env.MINIO_PORT) || 9000,
        useSSL: process.env.MINIO_USE_SSL === "1",
        accessKey: process.env.MINIO_ACCESS_KEY || "minio-access-key",
        secretKey: process.env.MINIO_SECRET_KEY || "minio-secret-key"
    },
    publicUrl: process.env.MINIO_PUBLIC_URL || "http://127.0.0.1:9000",
    regionName: process.env.MINIO_REGION_NAME || "minio-region-name",
    bucketName: process.env.LAUNCHSERVER_MINIO_BUCKET_NAME || "launchserver-minio-bucket-name"
});
const defaultAssetsService = new MojangDefaultAssetsService();
const launchServerService = new LaunchServerService({
    launcherPath: process.env.LAUNCHER_PATH || "files/launcher.jar",
    assetsPath: process.env.ASSETS_PATH || "files/assets.jar",
    filesPath: "files",
    clientsPath: "clients"
}, storageService, defaultAssetsService);
const tokenService = new JWTTokenService({
    jwtSecret: process.env.JWT_SECRET || getRandomJwtSecret(),
    accessTokenExpirationTime: parseInt(<string>process.env.ACCESS_TOKEN_EXPIRATION_TIME) || 24 * 60 * 60,
    refreshTokenExpirationTime: parseInt(<string>process.env.REFRESH_TOKEN_EXPIRATION_TIME) || 30 * 24 * 60 * 60
});
const postgreSQLDatabaseService = new PoolingPostgreSQLDatabaseService({
    host: process.env.DATABASE_HOST || "127.0.0.1",
    port: parseInt(<string>process.env.DATABASE_PORT) || 5432,
    user: process.env.DATABASE_USER || "database-user",
    password: process.env.DATABASE_PASSWORD || "database-password",
    database: process.env.DATABASE_DATABASE || "database-database"
});
const uuidService = new LocalTimeoutCacheCachingUuidService({
    timeout: parseInt(<string>process.env.UUID_CACHE_TIMEOUT) || (24 * 60 * 60 * 1000)
}, new PostgreSQLUuidService(postgreSQLDatabaseService));
const minecraftAuthService = new LocalCacheMinecraftAuthService(uuidService);
const profileImageStorageService = new MinioStorageService({
    minioClientOptions: {
        endPoint: process.env.MINIO_ENDPOINT || "127.0.0.1",
        port: parseInt(<string>process.env.MINIO_PORT) || 9000,
        useSSL: process.env.MINIO_USE_SSL === "1",
        accessKey: process.env.MINIO_ACCESS_KEY || "minio-access-key",
        secretKey: process.env.MINIO_SECRET_KEY || "minio-secret-key"
    },
    publicUrl: process.env.MINIO_PUBLIC_URL || "http://127.0.0.1:9000",
    regionName: process.env.MINIO_REGION_NAME || "minio-region-name",
    bucketName: process.env.PROFILE_IMAGE_MINIO_BUCKET_NAME || "profile-image-minio-bucket-name"
});
const profileImageService = new LocalTimeoutCacheCachingProfileImageService({
    timeout: parseInt(<string>process.env.PROFILE_IMAGE_CACHE_TIMEOUT) || (24 * 60 * 60 * 1000)
}, new HashingStorageProfileImageService(profileImageStorageService));
const profileService = new DefaultProfileService(profileImageService, uuidService);
const authService = new PostgreSQLAuthService({
    passwordSalt: process.env.PASSWORD_SALT || "password-salt",
    otpSalt: process.env.OTP_SALT || "otp-salt"
}, postgreSQLDatabaseService);
const newsService = new VKNewsService({
    vkAccessToken: process.env.NEWS_VK_ACCESS_TOKEN || "vk-access-token",
    ownerId: parseInt(<string>process.env.NEWS_VK_OWNER_ID) || 1,
    newsAmount: parseInt(<string>process.env.NEWS_AMOUNT) || 30,
    updateInterval: parseInt(<string>process.env.NEWS_UPDATE_INTERVAL_AMOUNT) || (60 * 60 * 1000),
});
const serverOnlineService = new RedisServerOnlineService({
    redisUrl: process.env.REDIS_URL || "redis://127.0.0.1:6379/"
});

const server = new WebServer();
server.addHandler("/api/auth", "POST", new AuthHandler(tokenService, authService));
server.addHandler("/api/launcher", "GET", new LauncherInfoHandler(launchServerService));
server.addHandler("/api/launcherAssets", "GET", new LauncherAssetsHandler(storageService, launchServerService));
server.addHandler("/api/token", "POST", new TokenRefreshHandler(tokenService, profileService));
server.addHandler("/api/news", "GET", new NewsHandler(newsService));
server.addHandler("/api/clients", "POST", new ClientsHandler(launchServerService, newsService,
    serverOnlineService, tokenService));
server.addHandler("/api/client", "POST", new ClientByIdHandler(launchServerService, tokenService));
server.addHandler("/api/minecraft/joinServer", "POST", new JoinServerHandler(tokenService, minecraftAuthService));
server.addHandler("/api/minecraft/hasJoined", "POST", new HasJoinedHandler(minecraftAuthService, profileService));
server.addHandler("/api/minecraft/profile/username", "POST", new ProfileByUsernameHandler(uuidService, profileService));
server.addHandler("/api/minecraft/profile/uuid", "POST", new ProfileByUuidHandler(profileService));
server.addHandler("/api/minecraft/profile/usernames", "POST", new ProfilesByUsernamesHandler(profileService));

(async () => {
    await Promise.all([storageService.initialize(), profileImageStorageService.initialize()]);
    server.start(parseInt(<string>process.env.SERVER_PORT) || 8080);
    launchServerService.start();
    newsService.start();
})().catch(error => {
    console.error(error);
    process.exit(1);
});
