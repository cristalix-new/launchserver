import {Uuid} from "../helpers/utils";

export interface AuthService {

    auth(usernameOrEmail: string, password: string, otpCode?: string): Promise<Uuid | null>;
}

export class AuthError extends Error {
    constructor(message: string) {
        super(message);
    }
}