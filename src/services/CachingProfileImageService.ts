import {Uuid} from "../helpers/utils";
import {ProfileImageService, ProfileImageType} from "./ProfileImageService";

export interface CachingProfileImageService extends ProfileImageService {

    invalidateImage(uuid: Uuid, type: ProfileImageType): void;
}