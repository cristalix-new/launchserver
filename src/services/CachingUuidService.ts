import {UuidService} from "./UuidService";
import {Uuid} from "../helpers/utils";

export interface CachingUuidService extends UuidService {

    invalidateUuid(uuid: Uuid): void;
}