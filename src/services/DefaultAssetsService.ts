import {AssetFile} from "./LaunchServerService";

export interface DefaultAssetsService {
    
    getAssets(version: string): Promise<AssetFile[]>;
}