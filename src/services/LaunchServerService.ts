import AsyncLock from "async-lock";
import {hashFromFile, xxHashFromFile} from "../helpers/hasher";
import {promises as fsPromises} from "fs";
import {ClientConfig, decodeConfig} from "../configs/ClientConfig";
import fastGlob from "fast-glob";
import path from "path";
import {StorageService} from "./StorageService";
import {DefaultAssetsService} from "./DefaultAssetsService";

export interface StorageConfig {
    readonly launcherPath: string;
    readonly assetsPath: string;
    readonly filesPath: string;
    readonly clientsPath: string;
}

export interface AssetFile {
    readonly downloadUrl: string;
    readonly path: string;
    readonly sha256: string;
    readonly size: number;
    readonly mojang: boolean;
}

interface ClientFile {
    readonly downloadUrl: string;
    readonly path: string;
    readonly sha256: string;
    readonly xxHash: string;
    readonly size: number;
}

interface Files {
    readonly clientFiles: ClientFile[];
    readonly assetFiles: AssetFile[];
}

interface Client extends Files {
    readonly config: ClientConfig;
}

interface LauncherInfo {
    readonly downloadUrl: string;
    readonly hash: string;
    readonly size: number;
}

export class LaunchServerService {
    private readonly storageLock = new AsyncLock();

    private launcherHash: string | null = null;
    private launcherSize: number | null = null;
    private assetsHash: string | null = null;

    private clientConfigs: Record<string, ClientConfig> = {};
    private clientFiles: Record<string, Files> = {};
    private readonly currentlySyncingClients = new Set<string>();
    private currentlyCleaningStorage = false;

    constructor(private readonly config: StorageConfig, private readonly storageService: StorageService,
                private readonly defaultAssetsService: DefaultAssetsService) {
    }

    start(): void {
        Promise.all([this.updateLauncherHash(), this.updateClientList()]).catch(console.error);
    }

    async updateLauncherHash(): Promise<void> {
        await this.storageLock.acquire("launcher", async () => {
            const launcherPath = `${this.config.launcherPath}`;
            this.launcherSize = (await fsPromises.stat(launcherPath)).size;
            if (this.launcherHash) {
                await this.storageService.removeFile(this.launcherHash);
            }
            this.launcherHash = await hashFromFile(launcherPath, "sha256");
            await this.storageService.uploadFileFromFS(launcherPath, this.launcherHash);
            const assetsPath = `${this.config.assetsPath}`;
            if (this.assetsHash) {
                await this.storageService.removeFile(this.assetsHash);
            }
            this.assetsHash = await hashFromFile(assetsPath, "sha256");
            await this.storageService.uploadFileFromFS(assetsPath, this.assetsHash);
        });
    }

    async updateClientList(): Promise<void> {
        await this.storageLock.acquire("clients", async () => {
            const files = await fsPromises.readdir(this.config.clientsPath);
            const newClients: Record<string, ClientConfig> = {};

            await Promise.all(files.map(async file => {
                const fullFilePath = `${this.config.clientsPath}/${file}`;

                if (!fullFilePath.endsWith(".json")) {
                    return;
                }

                try {
                    const clientConfigData = JSON.parse(await fsPromises.readFile(fullFilePath, {
                        encoding: "utf8"
                    }));

                    const clientConfig = decodeConfig(clientConfigData);

                    if (newClients[clientConfig.id]) {
                        console.error(`Failed to read client '${file}': Client '${clientConfig.id}' already exists`);
                        return;
                    }

                    newClients[clientConfig.id] = clientConfig;
                } catch (e) {
                    console.error(`Failed to read client '${file}': ${e}`);
                }
            }));

            this.clientConfigs = newClients;

            const unusedClientFiles: string[] = [];

            for (const clientId of Object.keys(this.clientFiles)) {
                if (!newClients[clientId]) {
                    unusedClientFiles.push(clientId);
                }
            }

            for (const clientId of unusedClientFiles) {
                delete this.clientFiles[clientId];
            }

            for (const clientId of Object.keys(newClients)) {
                if (!this.clientFiles[clientId]) {
                    this.tryStartClientFileSync(clientId);
                }
            }
        });
    }

    private tryStartClientFileSync(clientId: string): boolean {
        if (this.currentlySyncingClients.has(clientId)) {
            return false;
        }
        if (this.currentlyCleaningStorage) {
            return false;
        }
        this.currentlySyncingClients.add(clientId);
        const clientConfig = this.clientConfigs[clientId];
        if (!clientConfig) {
            throw new Error("no-such-client");
        }
        setTimeout(async () => {
            try {
                const startTime = new Date().getTime();
                const files = await this.getFiles(clientConfig);
                if (!this.clientConfigs[clientId]) {
                    return;
                }
                this.clientFiles[clientId] = files;
                console.info(`Client '${clientId}' synced successfully in ${new Date().getTime() - startTime} ms`);
            } catch (e) {
                console.error(`Failed to sync client '${clientId}': ${e}`);
            } finally {
                this.currentlySyncingClients.delete(clientId);
                if (this.currentlySyncingClients.size === 0) {
                    await this.cleanStorage();
                }
            }
        });
        return true;
    }

    private async cleanStorage() {
        console.info("Cleaning storage");
        const startTime = new Date().getTime();
        const allIndexedFiles = new Set(Object.keys(this.clientFiles).map(clientId =>
            [this.clientFiles[clientId].clientFiles.map(file => file.sha256),
                this.clientFiles[clientId].assetFiles.filter(file => !file.mojang)
                    .map(file => file.sha256)].flat()
        ).flat());
        const allStoredFiles = new Set(await this.storageService.getFilesList());
        for (const file of allIndexedFiles) {
            allStoredFiles.delete(file);
        }
        await Promise.all(Array.from(allStoredFiles).map(file => this.storageService.removeFile(file)));
        console.info(`Removed ${allStoredFiles.size} files from storage in ${new Date().getTime() - startTime} ms`);
    }

    private async getFiles(config: ClientConfig): Promise<Files> {
        const filesResult = {
            clientFiles: [] as ClientFile[],
            assetFiles: [] as AssetFile[]
        };

        await Promise.all([(async () => {
            const clientFilesPath = `${this.config.filesPath}/clients/${config.directory}`;
            const files = await fastGlob(`${clientFilesPath}/**/*`);
            await Promise.all(files.map(async file => {
                const hashes = await Promise.all([hashFromFile(file), xxHashFromFile(file)]);
                await this.storageService.uploadFileFromFS(file, hashes[0]);
                filesResult.clientFiles.push({
                    downloadUrl: this.storageService.getDownloadUrl(hashes[0]),
                    path: path.relative(clientFilesPath, file),
                    sha256: hashes[0],
                    xxHash: hashes[1],
                    size: (await fsPromises.stat(file)).size
                });
            }));
        })(), (async () => {
            const assetFilesPath = `${this.config.filesPath}/assets/${config.assetsAdditionsDirectory}`;
            const files = await fastGlob(`${assetFilesPath}/**/*`);
            const mojangAssets = await this.defaultAssetsService.getAssets(config.version);
            const availableMojangHashes = new Set(mojangAssets.map(assetFile => assetFile.sha256));
            const totalAssets: Record<string, AssetFile> = {};
            for (const assetFile of mojangAssets) {
                totalAssets[assetFile.path] = assetFile;
            }
            await Promise.all(files.map(async file => {
                const sha256 = await hashFromFile(file);
                const assetFilePath = path.relative(assetFilesPath, file);
                if (availableMojangHashes.has(sha256)) {
                    return;
                }
                await this.storageService.uploadFileFromFS(file, sha256);
                totalAssets[assetFilePath] = {
                    downloadUrl: this.storageService.getDownloadUrl(sha256),
                    path: assetFilePath,
                    sha256: sha256,
                    size: (await fsPromises.stat(file)).size,
                    mojang: false
                };
            }));
            filesResult.assetFiles = Object.keys(totalAssets).map(assetFilePath => ({
                downloadUrl: totalAssets[assetFilePath].downloadUrl,
                path: assetFilePath,
                sha256: totalAssets[assetFilePath].sha256,
                size: totalAssets[assetFilePath].size,
                mojang: totalAssets[assetFilePath].mojang
            }));
        })()]);

        return filesResult;
    }

    getLauncherInfo(): LauncherInfo {
        if (this.launcherHash === null || this.launcherSize === null || this.assetsHash === null) {
            throw new Error("no-launcher-found");
        }
        return {
            hash: this.launcherHash,
            size: this.launcherSize,
            downloadUrl: this.storageService.getDownloadUrl(this.launcherHash),
        };
    }

    getClientById(id: string): Client | null {
        return Object.keys(this.clientConfigs).filter(clientId => clientId === id)
            .filter(clientId => this.clientFiles[clientId])
            .map(clientId => ({
                config: this.clientConfigs[clientId],
                ...this.clientFiles[clientId]
            }))
            .find(() => true) ?? null;
    }

    getClients(): Client[] {
        return Object.keys(this.clientConfigs).filter(clientId => this.clientFiles[clientId])
            .map(clientId => ({
                config: this.clientConfigs[clientId],
                ...this.clientFiles[clientId]
            }));
    }

    getAssetsHash(): string | null {
        return this.assetsHash;
    }
}
