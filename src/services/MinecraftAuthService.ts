import {Uuid} from "../helpers/utils";

export interface MinecraftAuthService {

    hasJoined(username: string, serverId: string): Promise<string | null>;

    joinServer(uuid: Uuid, serverId: string): void;
}