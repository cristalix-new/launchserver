import {NewsItem} from "../handlers/NewsHandler";

export interface NewsService {

    fetchLauncherNews(): Promise<void>;

    getLauncherNews(): Promise<NewsItem[]>;
}