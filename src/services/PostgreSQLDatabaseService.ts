import {QueryConfig, QueryResult, QueryResultRow} from "pg";

export interface PostgreSQLDatabaseService {

    /* eslint-disable @typescript-eslint/no-explicit-any */
    query<R extends QueryResultRow = any, I extends any[] = any[]>(
        queryTextOrConfig: string | QueryConfig<I>,
        values?: I,
    ): Promise<QueryResult<R>>;
}