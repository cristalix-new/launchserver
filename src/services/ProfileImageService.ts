import {Uuid} from "../helpers/utils";

export interface ProfileImage {
    readonly url: string;
    readonly sha256: string;
}

export type ProfileImageType = "skin" | "cape";

export interface ProfileImageService {

    getProfileImage(uuid: Uuid, type: ProfileImageType): Promise<ProfileImage | null>;
}