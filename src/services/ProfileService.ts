import {Uuid} from "../helpers/utils";
import {ProfileImage} from "./ProfileImageService";

export interface ProfileData {
    readonly uuid: Uuid;
    readonly username: string;
    readonly skin: ProfileImage | null;
    readonly cape: ProfileImage | null;
}

export interface ProfileService {

    getProfileByUuid(uuid: Uuid): Promise<ProfileData | null>;

    getProfilesByUsernames(usernames: string[]): Promise<(ProfileData | null)[]>;
}