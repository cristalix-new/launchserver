export interface ServerOnlineData {
    readonly online: boolean;
    readonly currentPlayers: number;
    readonly maxPlayers: number;
}

export interface ServerOnlineService {

    getServerOnline(id: string): Promise<ServerOnlineData>;
}