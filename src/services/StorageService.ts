import {Stream} from "stream";

export interface StorageService {

    initialize(): Promise<void>;

    uploadFileFromFS(fileName: string, path: string): Promise<void>;

    getFile(path: string): Promise<Stream | null>;

    getFilesList(): Promise<string[]>;

    removeFile(path: string): Promise<void>;

    getDownloadUrl(path: string): string;
}
