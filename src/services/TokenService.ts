import {Uuid} from "../helpers/utils";

export interface TokenData {
    readonly uuid: Uuid;
    readonly purpose: "access" | "refresh";
}

export interface TokenService {

    createToken(tokenData: TokenData): string;

    getTokenData(token?: string): TokenData | null;
}