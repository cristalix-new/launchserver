import {Uuid} from "../helpers/utils";

export interface UuidService {

    uuidToUsername(uuid: Uuid): Promise<string | null>;

    usernameToUuid(username: string): Promise<Uuid | null>;

    usernamesToUuids(uuids: string[]): Promise<(Uuid | null)[]>;
}