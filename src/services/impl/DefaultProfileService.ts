import {UuidService} from "../UuidService";
import {Uuid} from "../../helpers/utils";
import {ProfileData, ProfileService} from "../ProfileService";
import {ProfileImageService} from "../ProfileImageService";

export class DefaultProfileService implements ProfileService {

    constructor(private readonly profileImageService: ProfileImageService,
                private readonly uuidService: UuidService) {
    }

    async getProfileByUuid(uuid: Uuid): Promise<ProfileData | null> {
        const username = await this.uuidService.uuidToUsername(uuid);
        if (!username) {
            return null;
        }

        const images = await Promise.all([this.profileImageService.getProfileImage(uuid, "skin"),
            this.profileImageService.getProfileImage(uuid, "cape")]);

        return {
            uuid,
            username,
            skin: images[0],
            cape: images[1]
        };
    }

    async getProfilesByUsernames(usernames: string[]): Promise<(ProfileData | null)[]> {
        const uuids = await this.uuidService.usernamesToUuids(usernames);

        return Promise.all(uuids.map(uuid => uuid ? this.getProfileByUuid(uuid) : null));
    }
}