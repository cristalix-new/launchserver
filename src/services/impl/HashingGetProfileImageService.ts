import {ProfileImage, ProfileImageService, ProfileImageType} from "../ProfileImageService";
import {formatString, Uuid} from "../../helpers/utils";
import axios from "axios";
import {Stream} from "stream";
import {hashFromStream} from "../../helpers/hasher";

export interface HashingGetProfileImageServiceConfig {
    readonly skinUrlFormat: string;
    readonly capeUrlFormat: string;
}

export class HashingGetProfileImageService implements ProfileImageService {

    constructor(private readonly config: HashingGetProfileImageServiceConfig) {
    }

    async getProfileImage(uuid: Uuid, type: ProfileImageType): Promise<ProfileImage | null> {
        try {
            let urlFormat: string;

            switch (type) {
            case "skin":
                urlFormat = this.config.skinUrlFormat;
                break;
            case "cape":
                urlFormat = this.config.capeUrlFormat;
                break;
            }

            if (!urlFormat) {
                return null;
            }

            const url = formatString(urlFormat, {
                uuid: uuid
            });

            const skinResponse = (await axios.get(url, {
                responseType: "stream"
            }));

            if (skinResponse.status != 200) {
                return null;
            }

            const skinStream = skinResponse.data as Stream;

            return {
                url: url,
                sha256: await hashFromStream(skinStream)
            };
        } catch (e) {
            return null;
        }
    }
}
