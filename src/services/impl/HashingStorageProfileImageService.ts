import {ProfileImage, ProfileImageService, ProfileImageType} from "../ProfileImageService";
import {Uuid} from "../../helpers/utils";
import {hashFromStream} from "../../helpers/hasher";
import {StorageService} from "../StorageService";

export class HashingStorageProfileImageService implements ProfileImageService {

    constructor(private readonly profileImageStorageService: StorageService) {
    }

    async getProfileImage(uuid: Uuid, type: ProfileImageType): Promise<ProfileImage | null> {
        try {
            const itemPath = `${type}/${uuid}`;

            const skinResponse = await this.profileImageStorageService.getFile(itemPath);

            if (!skinResponse) {
                return null;
            }

            return {
                url: this.profileImageStorageService.getDownloadUrl(itemPath),
                sha256: await hashFromStream(skinResponse)
            };
        } catch (e) {
            return null;
        }
    }
}
