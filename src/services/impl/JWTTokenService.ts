import jwt from "jsonwebtoken";
import {randomBytes} from "crypto";
import {Uuid} from "../../helpers/utils";
import {TokenService} from "../TokenService";

export interface TokenData {
    readonly uuid: Uuid;
    readonly purpose: "access" | "refresh";
}

export function getRandomJwtSecret(): string {
    return randomBytes(32).toString("hex");
}

export interface JWTTokenServiceConfig {
    readonly jwtSecret: string;
    readonly accessTokenExpirationTime: number;
    readonly refreshTokenExpirationTime: number;
}

export class JWTTokenService implements TokenService {

    constructor(private readonly config: JWTTokenServiceConfig) {
    }

    createToken(tokenData: TokenData): string {
        return jwt.sign(tokenData, this.config.jwtSecret, {
            expiresIn: tokenData.purpose === "access" ? this.config.accessTokenExpirationTime :
                this.config.refreshTokenExpirationTime
        });
    }

    getTokenData(token?: string): TokenData | null {
        try {
            if (!token) {
                return null;
            }
            return jwt.verify(token, this.config.jwtSecret) as TokenData;
        } catch (e) {
            return null;
        }
    }
}