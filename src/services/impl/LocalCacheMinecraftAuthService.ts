import {UuidService} from "../UuidService";
import {Uuid} from "../../helpers/utils";
import {MinecraftAuthService} from "../MinecraftAuthService";

export class LocalCacheMinecraftAuthService implements MinecraftAuthService {

    private readonly joinedCache: Record<string, string> = {};

    constructor(private readonly uuidService: UuidService) {
    }

    async hasJoined(username: string, serverId: string): Promise<string | null> {
        const uuid = await this.uuidService.usernameToUuid(username);
        if (!uuid) {
            return null;
        }
        return this.joinedCache[uuid] === serverId ? uuid : null;
    }

    joinServer(uuid: Uuid, serverId: string): void {
        this.joinedCache[uuid] = serverId;
    }
}