import {CachingProfileImageService} from "../CachingProfileImageService";
import {Uuid} from "../../helpers/utils";
import {ProfileImage, ProfileImageService, ProfileImageType} from "../ProfileImageService";

export interface LocalTimeoutCacheCachingProfileImageServiceConfig {
    readonly timeout: number;
}

export class LocalTimeoutCacheCachingProfileImageService implements CachingProfileImageService {

    private readonly profileImageCache: Record<ProfileImageType, Record<Uuid, ProfileImage | null>> = {
        skin: {},
        cape: {}
    };
    private readonly profileImageTimeoutCache: Record<ProfileImageType, Record<Uuid, NodeJS.Timeout>> = {
        skin: {},
        cape: {}
    }

    constructor(private readonly config: LocalTimeoutCacheCachingProfileImageServiceConfig,
                private readonly wrappedProfileImageService: ProfileImageService) {
    }

    private cacheProfileImage(uuid: Uuid, type: ProfileImageType, image: ProfileImage | null): void {
        this.profileImageCache[type][uuid] = image;
        if (!this.profileImageTimeoutCache[type][uuid]) {
            this.profileImageTimeoutCache[type][uuid] = setTimeout(() => this.invalidateImage(uuid, type),
                this.config.timeout);
        }
    }

    async getProfileImage(uuid: Uuid, type: ProfileImageType): Promise<ProfileImage | null> {
        if (this.profileImageCache[type][uuid] !== undefined) {
            return this.profileImageCache[type][uuid];
        }
        const profileImage = await this.wrappedProfileImageService.getProfileImage(uuid, type);
        this.cacheProfileImage(uuid, type, profileImage);
        return profileImage;
    }

    invalidateImage(uuid: Uuid, type: ProfileImageType): void {
        if (!this.profileImageCache[type][uuid]) {
            return;
        }
        delete this.profileImageCache[type][uuid];
        clearTimeout(this.profileImageTimeoutCache[type][uuid]);
        delete this.profileImageTimeoutCache[type][uuid];
    }
}