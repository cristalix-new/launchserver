import {CachingUuidService} from "../CachingUuidService";
import {Uuid} from "../../helpers/utils";
import {UuidService} from "../UuidService";

export interface LocalTimeoutCacheCachingUuidServiceConfig {
    readonly timeout: number;
}

export class LocalTimeoutCacheCachingUuidService implements CachingUuidService {
    private readonly usernameToUuidCache: Record<string, Uuid> = {};
    private readonly uuidToUsernameCache: Record<Uuid, string> = {};
    private readonly uuidTimeoutCache: Record<Uuid, NodeJS.Timeout> = {};

    constructor(private readonly config: LocalTimeoutCacheCachingUuidServiceConfig,
                private readonly wrappedUuidService: UuidService) {
    }

    invalidateUuid(uuid: Uuid): void {
        const username = this.uuidToUsernameCache[uuid];
        if (username != null) {
            delete this.usernameToUuidCache[username];
            delete this.uuidToUsernameCache[uuid];
            clearTimeout(this.uuidTimeoutCache[uuid]);
            delete this.uuidTimeoutCache[uuid];
        }
    }

    private cachePair(username: string, uuid: Uuid): void {
        this.usernameToUuidCache[username] = uuid;
        this.uuidToUsernameCache[uuid] = username;
        if (!this.uuidTimeoutCache[uuid]) {
            this.uuidTimeoutCache[uuid] = setInterval(() => this.invalidateUuid(uuid),
                this.config.timeout);
        }
    }

    async usernameToUuid(username: string): Promise<Uuid | null> {
        if (this.usernameToUuidCache[username]) {
            return this.usernameToUuidCache[username];
        }
        const uuid = await this.wrappedUuidService.usernameToUuid(username);
        if (uuid == null) {
            return null;
        }
        this.cachePair(username, uuid);
        return uuid;
    }

    async usernamesToUuids(uuids: string[]): Promise<(Uuid | null)[]> {
        return await Promise.all(uuids.map(async uuid => this.usernameToUuid(uuid)));
    }

    async uuidToUsername(uuid: Uuid): Promise<string | null> {
        if (this.uuidToUsernameCache[uuid]) {
            return this.uuidToUsernameCache[uuid];
        }
        const username = await this.wrappedUuidService.uuidToUsername(uuid);
        if (username == null) {
            return null;
        }
        this.cachePair(username, uuid);
        return username;
    }
}