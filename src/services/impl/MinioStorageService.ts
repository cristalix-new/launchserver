import {Client, ClientOptions, Region} from "minio";
import {StorageService} from "../StorageService";
import {Stream} from "stream";

export interface MinioStorageServiceConfig {
    readonly minioClientOptions: ClientOptions;
    readonly publicUrl: string;
    readonly bucketName: string;
    readonly regionName: Region;
}

export class MinioStorageService implements StorageService {
    private readonly client: Client;

    constructor(private readonly config: MinioStorageServiceConfig) {
        this.client = new Client(config.minioClientOptions);
    }

    async initialize(): Promise<void> {
        if (!await this.client.bucketExists(this.config.bucketName)) {
            await this.client.makeBucket(this.config.bucketName, this.config.regionName);
            await this.client.setBucketPolicy(this.config.bucketName, "download");
        }
    }

    async uploadFileFromFS(fileName: string, path: string): Promise<void> {
        try {
            await this.client.statObject(this.config.bucketName, path);
        } catch {
            await this.client.fPutObject(this.config.bucketName, path, fileName, {});
        }
    }

    async getFile(path: string): Promise<Stream | null> {
        try {
            return this.client.getObject(this.config.bucketName, path);
        } catch {
            return null;
        }
    }

    async getFilesList(): Promise<string[]> {
        return await new Promise((resolve, reject) => {
            const files = [] as string[];
            this.client.listObjects(this.config.bucketName)
                .on("data", (object) => files.push(object.name))
                .on("error", (err) => reject(err))
                .on("end", () => resolve(files))
                .on("close", () => resolve(files));
        });
    }

    async removeFile(path: string): Promise<void> {
        await this.client.removeObject(this.config.bucketName, path);
    }

    getDownloadUrl(path: string): string {
        return `${this.config.publicUrl}/${this.config.bucketName}/${path}`;
    }
}
