import axios from "axios";
import {AssetFile} from "../LaunchServerService";
import {DefaultAssetsService} from "../DefaultAssetsService";

interface MojangVersionManifestVersion {
    readonly id: string;
    readonly url: string;
}

interface MojangVersionManifestFile {
    readonly versions: MojangVersionManifestVersion[];
}

interface MojangVersionAssetIndex {
    readonly url: string;
}

interface MojangVersionFile {
    readonly assetIndex: MojangVersionAssetIndex;
}

interface MojangAssetIndexObjectData {
    readonly hash: string;
    readonly size: number;
}

interface MojangAssetIndexFile {
    readonly objects: Record<string, MojangAssetIndexObjectData>;
}

export class MojangDefaultAssetsService implements DefaultAssetsService {

    async getAssets(version: string): Promise<AssetFile[]> {
        const versionManifest = (await axios.get("https://launchermeta.mojang.com/mc/game/version_manifest.json"))
            .data as MojangVersionManifestFile;
        const selectedVersion = versionManifest.versions.find(otherVersion => otherVersion.id === version);
        if (!selectedVersion) {
            throw new Error(`Version '${version}' does not exist`);
        }
        const versionFile = (await axios.get(selectedVersion.url)).data as MojangVersionFile;
        const assetIndex = (await axios.get(versionFile.assetIndex.url)).data as MojangAssetIndexFile;
        return Object.keys(assetIndex.objects).map(path =>
            ({
                path: path,
                sha256: assetIndex.objects[path].hash,
                downloadUrl: `http://resources.download.minecraft.net/${assetIndex.objects[path]
                    .hash.substring(0, 2)}/${assetIndex.objects[path].hash}`,
                size: assetIndex.objects[path].size,
                mojang: true
            }));
    }
}