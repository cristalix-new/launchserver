import {Pool, QueryConfig, QueryResult, QueryResultRow} from "pg";
import {PostgreSQLDatabaseService} from "../PostgreSQLDatabaseService";

export interface PoolingPostgreSQLDatabaseServiceConfig {
    readonly host: string;
    readonly port: number;
    readonly user: string;
    readonly password: string;
    readonly database: string;
}

export class PoolingPostgreSQLDatabaseService implements PostgreSQLDatabaseService {

    private readonly pool: Pool;

    constructor(private readonly config: PoolingPostgreSQLDatabaseServiceConfig) {
        this.pool = new Pool({
            ...config
        });
    }

    /* eslint-disable @typescript-eslint/no-explicit-any */
    async query<R extends QueryResultRow = any, I extends any[] = any[]>(
        queryTextOrConfig: string | QueryConfig<I>,
        values?: I,
    ): Promise<QueryResult<R>> {
        return this.pool.query(queryTextOrConfig, values);
    }
}