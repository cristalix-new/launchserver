import {Uuid} from "../../helpers/utils";
import {PostgreSQLDatabaseService} from "../PostgreSQLDatabaseService";
import {createHash} from "crypto";
import {totp} from "speakeasy";
import {AuthError, AuthService} from "../AuthService";

export interface PostgreSQLAuthServiceConfig {
    readonly passwordSalt: string;
    readonly otpSalt: string;
}

interface AuthQueryResult {
    readonly id: number;
    readonly username: string;
    readonly uuid: Uuid;
    readonly password_hash: string;
    readonly ga_secret: string | null;
}

export class PostgreSQLAuthService implements AuthService {

    constructor(private readonly config: PostgreSQLAuthServiceConfig, private readonly postgreSQLDatabaseService: PostgreSQLDatabaseService) {
    }

    async auth(usernameOrEmail: string, password: string, otpCode?: string): Promise<Uuid | null> {
        let usernameColumn = "\"users\".\"username\"";
        if (usernameOrEmail.includes("@")) {
            usernameColumn = "\"credentials\".\"email\"";
        }
        const result = await this.postgreSQLDatabaseService.query<AuthQueryResult>(
            "SELECT \"users\".\"id\", \"users\".\"uuid\"::varchar, \"users\".\"username\", " +
            "\"credentials\".\"password_hash\", \"credentials\".\"ga_secret\" FROM \"users\" " +
            "LEFT JOIN \"credentials\" ON \"users\".\"id\" = \"credentials\".\"user_id\"" +
            `WHERE ${usernameColumn} = $1`, [usernameOrEmail]);

        if (result.rows.length === 0) {
            throw new AuthError("wrong-username-or-password");
        }

        const resultRow = result.rows[0];

        if (createHash("sha1").update(`${resultRow.id}${this.config.passwordSalt}${password}`).digest()
            .toString("hex") !== resultRow.password_hash) {
            throw new AuthError("wrong-username-or-password");
        }

        if (!resultRow.ga_secret) {
            return resultRow.uuid;
        }

        if (!otpCode) {
            throw new AuthError("otp-required");
        }

        if (!totp.verify({
            secret: `${this.config.otpSalt}${resultRow.id}${resultRow.ga_secret}`,
            encoding: "ascii",
            token: otpCode
        })) {
            throw new AuthError("wrong-otp-code");
        }

        return resultRow.uuid;
    }
}