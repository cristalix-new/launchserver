import {Uuid} from "../../helpers/utils";
import {PostgreSQLDatabaseService} from "../PostgreSQLDatabaseService";
import {UuidService} from "../UuidService";

export class PostgreSQLUuidService implements UuidService {

    constructor(private readonly postgreSQLDatabaseService: PostgreSQLDatabaseService) {
    }

    async uuidToUsername(uuid: Uuid): Promise<string | null> {
        const rows = (await this.postgreSQLDatabaseService.query("SELECT \"username\" FROM \"users\" WHERE \"uuid\" = $1::uuid", [uuid])).rows;
        return rows.length > 0 ? rows[0].username : null;
    }

    async usernameToUuid(username: string): Promise<Uuid | null> {
        const rows = (await this.postgreSQLDatabaseService.query("SELECT \"uuid\"::varchar FROM \"users\" WHERE \"username\" = $1", [username])).rows;
        return rows.length > 0 ? rows[0].uuid : null;
    }

    async usernamesToUuids(uuids: string[]): Promise<(Uuid | null)[]> {
        return await Promise.all(uuids.map(async uuid => this.usernameToUuid(uuid)));
    }
}