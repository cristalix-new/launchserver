import {createNodeRedisClient, WrappedNodeRedisClient} from "handy-redis";
import {ServerOnlineData, ServerOnlineService} from "../ServerOnlineService";

export interface RedisServerOnlineServiceConfig {
    readonly redisUrl: string;
}

export class RedisServerOnlineService implements ServerOnlineService {

    private readonly redis: WrappedNodeRedisClient;

    constructor(private readonly config: RedisServerOnlineServiceConfig) {
        this.redis = createNodeRedisClient({
            url: config.redisUrl
        });
        this.redis.nodeRedis.on("error", (e) => {
            console.error(e);
        });
    }

    async getServerOnline(id: string): Promise<ServerOnlineData> {
        return {
            online: parseInt(await this.redis.get(`server_${id}_is_online`) || "0") == 1,
            currentPlayers: parseInt(await this.redis.get(`server_${id}_current_players`) || "0"),
            maxPlayers: parseInt(await this.redis.get(`server_${id}_max_players`) || "0")
        };
    }
}