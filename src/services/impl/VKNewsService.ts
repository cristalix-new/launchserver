import {NewsItem} from "../../handlers/NewsHandler";
import {VK} from "vk-io";
import {PhotosPhoto} from "vk-io/lib/api/schemas/objects";
import {NewsService} from "../NewsService";

export interface VKNewsServiceConfig {
    readonly vkAccessToken: string;
    readonly ownerId: number;
    readonly newsAmount: number;
    readonly updateInterval: number;
}

export class VKNewsService implements NewsService {

    private news: NewsItem[] = [];

    constructor(private readonly config: VKNewsServiceConfig) {
    }

    start(): void {
        this.fetchLauncherNews().catch(console.error);
        setTimeout(() => this.fetchLauncherNews().catch(console.error), this.config.updateInterval);
    }

    async fetchLauncherNews(): Promise<void> {
        const vk = new VK({
            token: this.config.vkAccessToken
        });

        const posts = await vk.api.wall.get({
            owner_id: this.config.ownerId,
            count: this.config.newsAmount
        });

        const newNews = [] as NewsItem[];

        for (const post of posts.items) {
            if (!post.attachments || post.attachments.length === 0) {
                continue;
            }
            if (!post.date || !post.text) {
                continue;
            }
            let imageUrl: string | undefined;
            for (const attachment of post.attachments) {
                if (attachment.type !== "photo" || !attachment.photo) {
                    continue;
                }
                const photoAttachment = attachment.photo as PhotosPhoto;
                if (!photoAttachment || !photoAttachment.sizes) {
                    continue;
                }
                for (const size of photoAttachment.sizes) {
                    if (size.type === "y") {
                        imageUrl = size.url;
                        break;
                    }
                }
                if (imageUrl) {
                    break;
                }
            }
            if (!imageUrl) {
                continue;
            }
            newNews.push({
                timestamp: post.date,
                imageUrl: imageUrl,
                url: `https://vk.com/wall${post.owner_id}_${post.id}`,
                text: post.text
            });
        }

        this.news = newNews;
    }

    async getLauncherNews(): Promise<NewsItem[]> {
        return this.news;
    }
}