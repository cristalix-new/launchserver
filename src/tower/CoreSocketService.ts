import {EventEmitter} from "events";
import {WebSocketClient} from "./WebSocketClient";
import {IRealmID, RealmID} from "./RealmId";
import {
    CAN_EXECUTE_PACKAGE_CLASS,
    GREETING_PACKAGE_CLASS,
    ICanExecutePackage,
    ICapability,
    IGreetingPackage, IPackage,
    IWrappedPackage
} from "./packages";
import {v4} from "uuid";

type IHandler<T> = (data: T, source: RealmID) => void;

export interface CoreSocketConfig {
    readonly realmType: string;
    readonly realmId: number;
    readonly towerLogin: string;
    readonly towerPassword: string;
    readonly towerUrl: string;
}

export class CoreSocketService {
    private socket: WebSocketClient;
    private readonly local = new EventEmitter();

    constructor(private readonly config: CoreSocketConfig, connect?: (sock: CoreSocketService) => void) {
        this.local.setMaxListeners(50);
        this.socket = new WebSocketClient(this.config.towerUrl);
        this.socket.onOpen = () => {
            const realmId: IRealmID = {
                typeName: this.config.realmType,
                id: this.config.realmId
            };
            this.emit<IGreetingPackage>(GREETING_PACKAGE_CLASS, {
                login: this.config.towerLogin,
                password: this.config.towerPassword,
                realmId: realmId
            });
            this.sendCanExecute();
            if (connect) {
                connect(this);
            }
        };
        this.socket.onClose = () => {
            this.socket.open();
        };
        this.socket.onMessage = d => {
            const wrappedPackage: IWrappedPackage = JSON.parse(d.data as string);
            if (!wrappedPackage.objectData) {
                throw new Error(`Bad package object data: ${JSON.stringify(wrappedPackage)}`);
            }
            if (!wrappedPackage.className) {
                throw new Error(`Bad package class name: ${JSON.stringify(wrappedPackage)}`);
            }
            const data = JSON.parse(wrappedPackage.objectData);
            this.local.emit(wrappedPackage.className, data, wrappedPackage.source ?
                RealmID.fromString(wrappedPackage.source) : null);
        };
    }

    private readonly canExecute: ICapability[] = [];

    addCanExecute(canExecute: ICapability): void {
        this.canExecute.push(canExecute);
        this.sendCanExecute();
    }

    private sendCanExecute(): void {
        this.emit<ICanExecutePackage>(CAN_EXECUTE_PACKAGE_CLASS, {
            handled: this.canExecute
        });
    }

    on<T extends IPackage>(className: string, data: IHandler<T>): void {
        this.local.on(className, data);
    }

    off<T extends IPackage>(className: string, data: IHandler<T>): void {
        this.local.off(className, data);
    }

    once<T extends IPackage>(className: string, data: IHandler<T>): void {
        this.local.once(className, data);
    }

    emit<T extends IPackage>(className: string, data: T): void {
        if (!("id" in data)) {
            data.id = v4();
        }
        const stringData = JSON.stringify({
            className,
            objectData: JSON.stringify(data)
        });
        this.socket.send(stringData);
    }

    execute<Request extends IPackage, Response extends IPackage = Request>(
        className: string, data: Request, timeout = 15000): Promise<Response> {
        return new Promise((resolve, reject) => {
            if (!("id" in data)) {
                data.id = v4();
            }
            const listener = (pckg: Response) => {
                if (pckg.id !== data.id) return;
                resolve(pckg);
                this.off(className, listener);
            };
            setTimeout(() => {
                this.off(className, listener);
                reject(new Error(`Timeout exceeded for ${className} listener`));
            }, timeout);
            this.on(className, listener);
            this.emit(className, data);
        });
    }

    open(): void {
        this.socket.open();
    }

    close(): void {
        this.socket.close();
    }
}
