export class RealmID {
    private readonly typeName: string;
    private readonly id: number;

    constructor(realmId: IRealmID) {
        if (realmId.typeName.toUpperCase() !== realmId.typeName || realmId.typeName.length <= 1) {
            throw new Error(`Bad realm type name: ${realmId.typeName}`);
        }
        if (isNaN(realmId.id)) {
            throw new Error(`Bad realm id: ${realmId.id} (from ${JSON.stringify(realmId)})`);
        }
        this.typeName = realmId.typeName;
        this.id = realmId.id;
    }

    get isTestRealm(): boolean {
        return this.id < 0;
    }

    equals(other: IRealmID): boolean {
        return this.id === other.id && this.typeName === other.typeName;
    }

    toString(): string {
        if (this.isTestRealm) {
            return `${this.typeName}-TEST-${-this.id}`;
        }
        if (this.id > 0) {
            return `${this.typeName}-${this.id}`;
        }
        return this.typeName;
    }

    static fromString(name: string): RealmID {
        const parts = name.split("-");
        if (parts.length !== 2 && parts.length !== 3
            || parts.length === 3 && (parts[1] !== "TEST" || isNaN(parts[2] as unknown as number))
            || parts.length === 2 && isNaN(parts[1] as unknown as number)) {
            throw new Error(`Bad realm name: ${name}`);
        }
        if (parts.length === 3) {
            return new RealmID({
                typeName: parts[0],
                id: -parts[2]
            });
        }
        return new RealmID({
            typeName: parts[0],
            id: +parts[1]
        });
    }

    static fromInstance(instance: IRealmID | RealmID): RealmID {
        if (instance instanceof RealmID) {
            return instance;
        }
        return new RealmID(instance);
    }

    static toString(instance: IRealmID | RealmID): string {
        return RealmID.fromInstance(instance).toString();
    }
}

export interface IRealmID {
    typeName: string,
    id: number
}