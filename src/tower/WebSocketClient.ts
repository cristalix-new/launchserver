import WebSocket from "ws";

export class WebSocketClient {
    private number = 0;
    private readonly autoReconnectInterval: number;
    private readonly url: string;
    private instance: WebSocket | null = null;
    private safeClose = false;

    onOpen: () => void = () => {
        // default empty handler
    };
    onMessage: (event: WebSocket.MessageEvent, number: number) => void = () => {
        // default empty handler
    };
    onError: (event: WebSocket.ErrorEvent) => void = () => {
        // default empty handler
    };
    onClose: (event: WebSocket.CloseEvent) => void = () => {
        // default empty handler
    };

    constructor(url: string, reconnectInterval = 100) {
        this.autoReconnectInterval = reconnectInterval; // ms
        this.url = url;
    }

    open(): void {
        this.safeClose = false;
        this.instance = new WebSocket(this.url);
        this.instance.binaryType = "arraybuffer";
        this.instance.onopen = () => {
            this.onOpenResend();
            this.onOpen();
        };
        this.instance.onmessage = (event: WebSocket.MessageEvent) => {
            this.number++;
            this.onMessage(event, this.number);
        };
        this.instance.onclose = (event) => {
            if (!this.safeClose) {
                switch (event.code) {
                case 1000: // CLOSE_NORMAL
                    this.onClose(event);
                    break;
                default: // Abnormal closure
                    this.reconnect();
                    break;
                }
            } else {
                this.onClose(event);
            }
        };
        this.instance.onerror = (event) => {
            if (!this.safeClose) {
                this.reconnect();
            } else {
                this.onError(event);
            }
        };
    }

    close(): void {
        this.safeClose = true;
        this.instance?.close();
    }

    private sendBuffer: (string | Buffer)[] = [];

    send(data: string | Buffer): void {
        if (this.instance) {
            this.instance.send(data);
        } else {
            this.sendBuffer.push(data);
        }
    }

    reconnect(): void {
        if (!this.safeClose) {
            setTimeout(() => {
                this.open();
            }, this.autoReconnectInterval);
        }
    }

    onOpenResend(): void {
        for (const data of this.sendBuffer) {
            this.send(data);
        }
        this.sendBuffer = [];
    }
}
