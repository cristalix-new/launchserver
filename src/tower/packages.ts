import {IRealmID} from "./RealmId";

export interface IWrappedPackage {
    className?: string;
    objectData?: string;
    source?: string;
}

export interface IPackage {
    id?: string
}

export const CAN_EXECUTE_PACKAGE_CLASS = "ru.cristalix.core.network.packages.CanExecutePackage";

export interface ICanExecutePackage extends IPackage {
    handled: ICapability[]
}

export interface ICapability {
    className: string,
    notification: boolean,
    multi: boolean
}

export const GREETING_PACKAGE_CLASS = "pw.lach.cristalix.core.realm.packages.GreetingPackage";

export interface IGreetingPackage extends IPackage {
    login?: string,
    password?: string,
    realmId?: IRealmID
}

export const FILL_LAUNCHER_USER_DATA_PACKAGE_CLASS = "pw.lach.cristalix.core.account.packages.FillLauncherUserDataPackage";

export interface FillLauncherUserDataPackage extends IPackage {
    uuidList?: (string | null)[],
    usernameList?: (string | null)[],
}

