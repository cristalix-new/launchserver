declare module "xxhash-addon" {
    class XXHash128 {
        constructor();

        update(buffer: Buffer | string);
        digest(): Buffer;
        reset(): void;
    }

    class XXHash64 {
        constructor();

        update(buffer: Buffer | string);
        digest(): Buffer;
        reset(): void;
    }
}